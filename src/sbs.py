import sys
from src.sbsProject import Project
import random

DEFAULT_KEY = 4;
global c;

def storeInput(prompt):
    global c;
    c = input(prompt);
    return c;

def decrypt(filename):
    with open(filename) as project:

        key=DEFAULT_KEY; # Set default key, justincase. Not that it'll do you any good, I'm just doing this to shut my linter up.
        p = Project(filename.replace(".sbsp",""));

        for i, line in enumerate(project):
            templine=""; # Create new variable to store the line that is currently being decrypted character by character.
            if i==0:    # If we are still on the first line...
                key=line[0:2]
                key=int(int(key)>>DEFAULT_KEY); #set the key to the decryption key on the first line.
            else:
                data = line.split(" ") ;
                for orded in data: #Otherwise, for every character in the line...
                    templine+=str(chr(int(orded[0:3])>>key)); # Caesar-Cipher that char (bitwise shift by the key).

                print(templine);

                if (templine.startswith(">- ")):
                    templine = templine.replace(">- ", "");
                    p.addDataLine(i, templine);  # Append the newly decrypted line to the list of dataLines to add to the project.
                elif(templine.startswith(">+ ")):
                    templine = templine.replace(">+ ", "");
                    p.addSupplementData(templine);
        return p;

def encryptAndWrite(filename, proj):
    key = (random.randint(1, 4) << DEFAULT_KEY);
    with open(filename, "w") as file:
        file.write(str(key)+"\n");
        for line in range(len(proj.dataLine)):

            unencryptedData = ">- "+proj.dataLine[line].value;
            encryptedData = "";

            for char in unencryptedData:
                encryptedData += (str(ord(char)<<key)+" ");

            file.write(encryptedData+"\n");

            for p in range(len(proj.dataLine[line].points)):

                unencryptedData = ">+ "+str(line)+"|"+proj.dataLine.points[p];
                encryptedData = "";

                for char in unencryptedData:
                    encryptedData += (str(ord(char) << key)+" ");

                file.write(encryptedData+"\n");

            file.close();

def openProject(filename):
    p = decrypt(filename);
    editProject(p)

def editProject(project: Project):
    global c;
    while('x' not in storeInput("Currently Open: "+project.name+"\n"
                            "-x\tBack to top menu\n"
                            "-h\tPrint \"Help\" menu\n"
                            ": ")):
        if('h' in  c):
            print(
            "Available Commands:\n"
            "h\tDisplay this dialog\n"
            "ld\tDelete a Line\n"
            "ln\tAdd a New Line\n"
            "ls\tSelect a Line to move into#\n"
            ": "
            );
        elif('l' in  c):
            if('d' in c):
                actionCompleted = False;
                while(actionCompleted==False):
                    c=input("Delete which line?\n"
                            "-c\tCancel action\n"
                            "-p\tPrint all lines\n"
                            "#\tDelete line #\n"
                            ": ");
                    if('p' in c):
                        project.printData(True,False);
                    elif('c' in c):
                        actionCompleted=True;
                    else:
                        project.deleteLine(int(c));
                        actionCompleted=True;
            if ('n' in c):
                actionCompleted = False;
                while (actionCompleted == False):
                    i = input("Enter the new sentence (-c to cancel): [");
                    if(i.startswith("-c")):
                        actionCompleted=True;
                    elif(i == ""):
                        actionCompleted=False;
                    else:
                        project.addDataLine(len(project.dataLine), i);
                        actionCompleted = True;
            if('s' in c):
                actionCompleted=False;
                while(actionCompleted==False):
                    uIn = input("Select a line to work on:\n"
                                "-c\tCancel\n"
                                "-p\tPrint\n"
                                "#\tLine  to select\n"
                                ": ");
                    if('c' in uIn):
                        actionCompleted=True;
                    elif('p' in uIn):
                        project.printData(True, False);
                    else:
                        actionCompleted=True;
                        done=False;
                        curLine=int(uIn);
                        while(done==False):
                            storeInput(project.dataLine[curLine].value+"\n"+str(curLine)+":");
                            if('.' in c):
                                done=True;
                            if('p' in c):
                                if('r' in c or 'd' in c):
                                    subDone=False;
                                    while(subDone==False):
                                        uIn = input("Select a line to work on:\n"
                                                    "-c\tCancel\n"
                                                    "-p\tPrint\n"
                                                    "#\tPoint to Delete\n"
                                                    ": ");
                                        if ('c' in uIn):
                                            subDone = True;
                                        elif ('p' in uIn):
                                            project.printPointsAtLine(curLine);
                                        else:
                                            project.deletePoint(curLine, int(uIn));
                                elif('n' in c):
                                    project.dataLine[curLine].addPoint(input("Add a new Point: "));
                                else:
                                    print("Unknown Command.");

    closeProject(project.name+".sbsp", project);








def newProject(filename):
    with open(filename, "w") as project:
        project.write(str(random.randint(1,4))+"\n");
    p = Project(filename.replace(".sbsp",""));
    editProject(p);

def closeProject(filename, proj):
    encryptAndWrite(filename, proj);

while("q" not in storeInput("-n\tNew Project\n"
                            "-o\tOpen Project\n"
                            "-q\tQuit\n"
                            ":")):
    if("n" in c):
        projectName = input("Name of new project?: ");
        newProject(projectName+".sbsp");
    elif("o" in c):
        projectName = input("Name of project?: ");
        openProject(projectName + ".sbsp");


