from src.sbsLine import Line

class Project:
    #line is a dictionary composed of tuples formatted as (lineNumber, value)
    dataLine = [];
    name = "";
    length = 0;

    def __init__(self, name):
        self.name = name;
        self.length = 0;
        self.dataLine = [];

    def addDataLine(self, linenum, line):
        l = Line(linenum, line);
        self.dataLine.append(l);

    def deleteLine(self, linenum):
        pass;

    def deletePoint(self, line, point):
        pass;

    def addSupplementData(self, line):
        data=line.split("|");
        linenum = data[0];
        value = data[1];

        self.dataLine[linenum].addPoint(value);

    def printData(self, lineNumbers, printPoints):
        if(lineNumbers):
            for i in range(len(self.dataLine)):
                print(str(i) + ". " + self.dataLine[i].value);
                if(printPoints):
                    for p in range(len(self.dataLine[i].points)):
                        print(str(p) + ". " + self.dataLine[i].points[p]);

    def printPointsAtLine(self,line):
        for p in range(len(self.dataLine[line].points)):
            print(str(p) + ". " + self.dataLine[line].points[p]);